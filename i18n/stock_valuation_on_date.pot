# Translation of Odoo Server.
# This file contains the translation of the following modules:
#	* stock_valuation_on_date
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 8.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-12-23 04:21+0000\n"
"PO-Revision-Date: 2017-12-23 04:21+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: stock_valuation_on_date
#: view:stock.valuation.ondate.report:stock_valuation_on_date.view_stock_valuation_ondate_report
msgid "(If you do not select any categories it will take all categories)"
msgstr ""

#. module: stock_valuation_on_date
#: view:stock.valuation.ondate.report:stock_valuation_on_date.view_stock_valuation_ondate_report
msgid "(If you do not select any location, then it will take all above warehouses internal locations)"
msgstr ""

#. module: stock_valuation_on_date
#: view:stock.valuation.ondate.report:stock_valuation_on_date.view_stock_valuation_ondate_report
msgid "(If you do not select company it will take all companies)"
msgstr ""

#. module: stock_valuation_on_date
#: view:stock.valuation.ondate.report:stock_valuation_on_date.view_stock_valuation_ondate_report
msgid "(If you do not select warehouse it will take all warehouses)"
msgstr ""

#. module: stock_valuation_on_date
#: view:website:stock_valuation_on_date.stock_valuation_ondate_report
msgid "Adjustments"
msgstr ""

#. module: stock_valuation_on_date
#: view:website:stock_valuation_on_date.stock_valuation_ondate_report
msgid "Amount In Currency"
msgstr ""

#. module: stock_valuation_on_date
#: view:website:stock_valuation_on_date.stock_valuation_ondate_report
msgid "Beginning"
msgstr ""

#. module: stock_valuation_on_date
#: view:stock.valuation.ondate.report:stock_valuation_on_date.view_stock_valuation_ondate_report
msgid "Cancel"
msgstr ""

#. module: stock_valuation_on_date
#: field:stock.valuation.ondate.report,filter_product_categ_ids:0
msgid "Categories"
msgstr ""

#. module: stock_valuation_on_date
#: view:stock.valuation.ondate.report:stock_valuation_on_date.view_stock_valuation_ondate_report
#: field:stock.valuation.ondate.report,company_id:0
#: view:website:stock_valuation_on_date.stock_valuation_ondate_report
msgid "Company"
msgstr ""

#. module: stock_valuation_on_date
#: view:website:stock_valuation_on_date.stock_valuation_ondate_report
msgid "Cost"
msgstr ""

#. module: stock_valuation_on_date
#: field:stock.valuation.ondate.report,create_uid:0
#: field:stock.valuation.success.box,create_uid:0
msgid "Created by"
msgstr ""

#. module: stock_valuation_on_date
#: field:stock.valuation.ondate.report,create_date:0
#: field:stock.valuation.success.box,create_date:0
msgid "Created on"
msgstr ""

#. module: stock_valuation_on_date
#: field:stock.valuation.ondate.report,only_summary:0
msgid "Display Only Summary?"
msgstr ""

#. module: stock_valuation_on_date
#: view:website:stock_valuation_on_date.stock_valuation_ondate_report
msgid "Ending"
msgstr ""

#. module: stock_valuation_on_date
#: field:stock.valuation.success.box,file:0
msgid "File"
msgstr ""

#. module: stock_valuation_on_date
#: field:stock.valuation.ondate.report,start_date:0
msgid "From Date"
msgstr ""

#. module: stock_valuation_on_date
#: field:report.stock_valuation_on_date.stock_valuation_ondate_report,id:0
#: field:stock.valuation.ondate.report,id:0
#: field:stock.valuation.success.box,id:0
msgid "ID"
msgstr ""

#. module: stock_valuation_on_date
#: view:website:stock_valuation_on_date.stock_valuation_ondate_report
msgid "Internal"
msgstr ""

#. module: stock_valuation_on_date
#: view:website:stock_valuation_on_date.stock_valuation_ondate_report
msgid "Inventory Valuation Report"
msgstr ""

#. module: stock_valuation_on_date
#: field:stock.valuation.ondate.report,write_uid:0
#: field:stock.valuation.success.box,write_uid:0
msgid "Last Updated by"
msgstr ""

#. module: stock_valuation_on_date
#: field:stock.valuation.ondate.report,write_date:0
#: field:stock.valuation.success.box,write_date:0
msgid "Last Updated on"
msgstr ""

#. module: stock_valuation_on_date
#: view:stock.valuation.ondate.report:stock_valuation_on_date.view_stock_valuation_ondate_report
#: field:stock.valuation.ondate.report,location_id:0
msgid "Location"
msgstr ""

#. module: stock_valuation_on_date
#: code:addons/stock_valuation_on_date/wizard/stock_valuation.py:127
#, python-format
msgid "Please select company of those warehouses to get correct view.\n"
"You should remove all warehouses first from selection field."
msgstr ""

#. module: stock_valuation_on_date
#: view:stock.valuation.ondate.report:stock_valuation_on_date.view_stock_valuation_ondate_report
#: field:stock.valuation.ondate.report,filter_product_ids:0
msgid "Products"
msgstr ""

#. module: stock_valuation_on_date
#: view:website:stock_valuation_on_date.stock_valuation_ondate_report
msgid "Received"
msgstr ""

#. module: stock_valuation_on_date
#: view:website:stock_valuation_on_date.stock_valuation_ondate_report
msgid "Sales"
msgstr ""

#. module: stock_valuation_on_date
#: view:stock.valuation.ondate.report:stock_valuation_on_date.view_stock_valuation_ondate_report
msgid "Select Categories"
msgstr ""

#. module: stock_valuation_on_date
#: view:stock.valuation.ondate.report:stock_valuation_on_date.view_stock_valuation_ondate_report
msgid "Select Location"
msgstr ""

#. module: stock_valuation_on_date
#: model:ir.actions.act_window,name:stock_valuation_on_date.action_inventoryvaluation_report
#: model:ir.ui.menu,name:stock_valuation_on_date.menu_inventoryvaluation_report
msgid "Stock Valuation"
msgstr ""

#. module: stock_valuation_on_date
#: model:res.groups,name:stock_valuation_on_date.group_stock_valuation_features
msgid "Stock Valuation Features"
msgstr ""

#. module: stock_valuation_on_date
#: model:ir.actions.report.xml,name:stock_valuation_on_date.action_stock_valuation_ondate
msgid "Stock Valuation Report"
msgstr ""

#. module: stock_valuation_on_date
#: field:stock.valuation.success.box,fname:0
msgid "Text"
msgstr ""

#. module: stock_valuation_on_date
#: field:stock.valuation.ondate.report,end_date:0
msgid "To Date"
msgstr ""

#. module: stock_valuation_on_date
#: view:website:stock_valuation_on_date.stock_valuation_ondate_report
msgid "Total Inventory"
msgstr ""

#. module: stock_valuation_on_date
#: view:website:stock_valuation_on_date.stock_valuation_ondate_report
msgid "Total Value"
msgstr ""

#. module: stock_valuation_on_date
#: help:stock.valuation.ondate.report,only_summary:0
msgid "True, it will display only total summary of categories."
msgstr ""

#. module: stock_valuation_on_date
#: view:stock.valuation.ondate.report:stock_valuation_on_date.view_stock_valuation_ondate_report
msgid "Valuation"
msgstr ""

#. module: stock_valuation_on_date
#: view:website:stock_valuation_on_date.stock_valuation_ondate_report
msgid "Valuation Date"
msgstr ""

#. module: stock_valuation_on_date
#: model:ir.ui.menu,name:stock_valuation_on_date.menu_inventoryvaluation_main
#: view:stock.valuation.ondate.report:stock_valuation_on_date.view_stock_valuation_ondate_report
msgid "Valuation Report"
msgstr ""

#. module: stock_valuation_on_date
#: view:website:stock_valuation_on_date.stock_valuation_ondate_report
msgid "Warehouse"
msgstr ""

#. module: stock_valuation_on_date
#: view:stock.valuation.ondate.report:stock_valuation_on_date.view_stock_valuation_ondate_report
msgid "Warehouses"
msgstr ""

#. module: stock_valuation_on_date
#: view:stock.valuation.ondate.report:stock_valuation_on_date.view_stock_valuation_ondate_report
msgid "_PDF"
msgstr ""

#. module: stock_valuation_on_date
#: view:stock.valuation.ondate.report:stock_valuation_on_date.view_stock_valuation_ondate_report
msgid "_XLS"
msgstr ""

#. module: stock_valuation_on_date
#: view:stock.valuation.ondate.report:stock_valuation_on_date.view_stock_valuation_ondate_report
msgid "or"
msgstr ""

#. module: stock_valuation_on_date
#: field:stock.valuation.ondate.report,warehouse_ids:0
msgid "warehouse"
msgstr ""

